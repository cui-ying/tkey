import org.junit.Test;

import java.util.regex.Pattern;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Cui Ying
 */
public class RegexTest {

    @Test
    public void testRegex() {
        String regex = "^(http://sso.baicaowei.cn:9095/sso-client-management/|http://sso.baicaowei.cn:9092/).*$";
        boolean isMatch = Pattern.matches(regex, "http://sso.baicaowei.cn:9092/");
        assertTrue(isMatch);

        boolean isMatchc = Pattern.matches(regex, "http://sso.baicaowei.cn:909");
        assertFalse(isMatchc);

        boolean isMatchb = Pattern.matches(regex, "http://sso.baicaowei.cn:9092/a/b.htm");
        assertTrue(isMatchb);
    }
}
