package com.cdk8s.tkey.server.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Cui Ying
 */
@Controller
public class IndexController {

    @RequestMapping("/")
    public String viewIndex(Model model, String msg){

        model.addAttribute("msg", msg);
        return "index";
    }
}
