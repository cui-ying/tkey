package com.cdk8s.tkey.server.retry;


import com.bcw.sso.model.dto.SsoClientRedisDO;
import com.bcw.sso.model.dto.SsoCommonUser;
import com.cdk8s.tkey.server.pojo.dto.param.OauthFormLoginParam;
import com.cdk8s.tkey.server.service.LoginAccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;


@Service
@Slf4j
public class RetryService {

    @Autowired
    private LoginAccountService loginAccountService;


    @Retryable(value = {Exception.class}, maxAttempts = 2, backoff = @Backoff(delay = 2000L, multiplier = 1))
    public SsoCommonUser getOauthUserAttributeBO(String username, String password) {
        return loginAccountService.findLoginAccount(username, password);

    }

    @Retryable(value = {Exception.class}, maxAttempts = 2, backoff = @Backoff(delay = 2000L, multiplier = 1))
    public SsoCommonUser getOauthUserAttributeBO(OauthFormLoginParam oauthFormLoginParam, SsoClientRedisDO ssoClientToRedisBO) {
        return loginAccountService.findLoginAccount(oauthFormLoginParam, ssoClientToRedisBO);
    }

    @Recover
    public SsoCommonUser getOauthUserAttributeBORecover(Exception e) {
        log.error("多次重试调用验证用户名密码接口失败=<{}>", e.getMessage());
        return new SsoCommonUser();
    }

}
