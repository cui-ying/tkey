package com.cdk8s.tkey.server.service;

import com.bcw.sso.model.dto.SsoClientRedisDO;
import com.bcw.sso.model.pojo.SsoClient;
import com.cdk8s.tkey.server.constant.GlobalVariable;
import com.cdk8s.tkey.server.exception.OauthApiException;
import com.cdk8s.tkey.server.mapper.SsoClientMapper;
import com.cdk8s.tkey.server.pojo.dto.param.SsoClientPageQueryParam;
import com.cdk8s.tkey.server.util.JsonUtil;
import com.cdk8s.tkey.server.util.StringUtil;
import com.cdk8s.tkey.server.util.redis.StringRedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@Slf4j
public class SsoClientService {
    @Autowired
    private SsoClientMapper ssoClientMapper;

    /**
     * 这里必须采用 value 为 String 类型，不然 客户端和服务端都要读取该信息，但是又没有共同的类进行序列化，所以必须转换成 JSON 字符串
     * 如果后面出现的公共类越来越多我再考虑独立出一个 jar 包出来维护
     */
    @Autowired
    private StringRedisService<String, String> clientRedisService;

    public SsoClientRedisDO findByClientId(String clientId) {
        // fixme: 客户端信息是在客户端管理系统上添加的，如果在那里修改后，没有更新缓存中的数据，因为缓存数据设置的是持久的，可能会存在更新后数据不一致问题。这个需要SSO服务端和SSO客户端管理平台两边缓存数据一致。
        String clientIdRedisKey = GlobalVariable.REDIS_CLIENT_ID_KEY_PREFIX + clientId;
        String result = clientRedisService.get(clientIdRedisKey);
        if (StringUtil.isBlank(result)) {
            SsoClientPageQueryParam query = new SsoClientPageQueryParam();
            query.setClientId(clientId);
            List<SsoClient> oauthClients = ssoClientMapper.selectByPageQueryParam(query);
            switch (oauthClients.size()) {
                case 1:
                    SsoClient oauthClient = oauthClients.get(0);
                    SsoClientRedisDO bo = new SsoClientRedisDO();
                    BeanUtils.copyProperties(oauthClient, bo);

                    clientRedisService.set(clientIdRedisKey, JsonUtil.toJson(bo));
                    return bo;
                case 0:
                    throw new OauthApiException("client_id 不存在");
                default:
                    throw new OauthApiException("client_id 超过1个了！！！");
            }

        } // else
        return JsonUtil.toObject(result, SsoClientRedisDO.class);
    }
}
