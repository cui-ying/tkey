package com.cdk8s.tkey.server.service;

import com.bcw.sso.model.dto.SsoClientRedisDO;
import com.bcw.sso.model.dto.SsoCommonUser;
import com.cdk8s.tkey.server.pojo.dto.param.OauthFormLoginParam;

/**
 * 登录账户服务
 *
 * @author ying
 */
public interface LoginAccountService {
    SsoCommonUser findLoginAccount(String username, String password);
    SsoCommonUser findLoginAccount(OauthFormLoginParam oauthFormLoginParam, SsoClientRedisDO ssoClientToRedisBO);
}
