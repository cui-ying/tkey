package com.cdk8s.tkey.server.util;

/**
 * MD5 加密
 * @author Cui Ying
 */
public class SsoUserPwdUtil {

    /**
     * 加密密码
     * @param username 使用用户名作为盐
     * @param metaPassword 密码元数据
     * @return 加密后的密码
     */
    public static String encryptPwd(String username, String metaPassword) {
        return Md5Util.md5AndHex(username, metaPassword, 2);
    }

}
