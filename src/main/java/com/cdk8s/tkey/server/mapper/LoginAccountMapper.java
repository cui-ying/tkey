package com.cdk8s.tkey.server.mapper;

import com.cdk8s.tkey.server.config.CustomBaseMapper;
import com.cdk8s.tkey.server.pojo.entity.LoginAccount;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author ying
 */
@Repository
public interface LoginAccountMapper extends CustomBaseMapper<LoginAccount> {
    /**
     * 根据用户名或手机号等唯一标识，查找有效的登录账户
     *
     * @param username 用户名 / 手机号
     * @return 有效登录账户
     */
    List<LoginAccount> findForLogin(String username);
}
