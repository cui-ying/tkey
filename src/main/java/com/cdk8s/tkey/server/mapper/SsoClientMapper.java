package com.cdk8s.tkey.server.mapper;

import com.bcw.sso.model.pojo.SsoClient;
import com.cdk8s.tkey.server.config.CustomBaseMapper;
import com.cdk8s.tkey.server.pojo.dto.param.SsoClientPageQueryParam;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author ying
 */
@Repository
public interface SsoClientMapper extends CustomBaseMapper<SsoClient> {

    /**
     * 分页多条件查询客户端信息
     *
     * @param queryParam 请求参数
     * @return 客户端信息
     */
    List<SsoClient> selectByPageQueryParam(SsoClientPageQueryParam queryParam);
}
