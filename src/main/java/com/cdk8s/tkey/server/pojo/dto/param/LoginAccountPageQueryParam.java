package com.cdk8s.tkey.server.pojo.dto.param;

import com.cdk8s.tkey.server.pojo.dto.param.bases.PageParam;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


/**
 * 登录账户分页查询器
 * @author ying
 */
@Setter
@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoginAccountPageQueryParam extends PageParam {

	private static final long serialVersionUID = -4304836933108852485L;

	private String username;
	private String phoneNumber;

}
