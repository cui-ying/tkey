package com.cdk8s.tkey.server.pojo.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 登录账户
 */
@Getter
@Setter
@ToString
@Table(name = "login_account")
public class LoginAccount {
	/**
	 * 主键ID
	 */
	@Id
	@Column(name = "id")
	private Integer id;

	/**
	 * 用户名
	 */
	@Column(name = "username")
	private String username;

	/**
	 * 密码
	 */
	@Column(name = "sso_password")
	private String ssoPassword;

	/**
	 * 手机号码
	 */
	@Column(name = "phone_number")
	private String phoneNumber;

	/**
	 * 来自哪个客户端ID
	 */
	@Column(name = "from_client_id")
	private Integer fromClientId;

	/**
	 * 状态
	 */
	@Column(name = "status")
	private String status;

}
