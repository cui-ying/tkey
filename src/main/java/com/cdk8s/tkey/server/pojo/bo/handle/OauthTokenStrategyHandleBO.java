package com.cdk8s.tkey.server.pojo.bo.handle;

import com.bcw.sso.model.dto.SsoCommonUser;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class OauthTokenStrategyHandleBO {
	private String userInfoRedisKey;
	private SsoCommonUser ssoCommonUser;
}
